<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {

        $data = [
            'email' => "example" . str_random(3) . "@example.com",
            'name' => 'test',
            'surname' => 'test',
            'birth_date' => '2000-08-06 20:17:29',
        ];

        $response = $this->json('POST', '/api/auth/register', $data + ['password' => '12345678']);

        $response
            ->assertStatus(201)
            ->assertJson($data);
    }
}
