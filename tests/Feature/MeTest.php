<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MeTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $email = "example" . str_random(3) . "@example.com";

        $data = [
            'email' => $email,
            'name' => 'test',
            'surname' => 'test',
            'birth_date' => '2000-08-06 20:17:29',
        ];

        $this->json('POST', '/api/auth/register', $data + ['password' => '12345678']);

        $user = User::where('email', $email)->first();

        $user->activate_token = null;
        $user->save();

        $response = $this
            ->actingAs($user)
            ->json('GET', '/api/me');

        $response
            ->assertJson($data)
            ->assertStatus(200);
    }
}
