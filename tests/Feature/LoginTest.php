<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLoginWithActivationTest()
    {
        $email = "example" . str_random(3) . "@example.com";

        $data = [
            'email' => $email,
            'name' => 'test',
            'surname' => 'test',
            'birth_date' => '2000-08-06 20:17:29',
        ];

        $this->json('POST', '/api/auth/register', $data + ['password' => '12345678']);


        $user = User::where('email', $email)->update(['activate_token' => null]);

        $response = $this->json('POST', '/api/auth/login', $data + ['password' => '12345678']);
        $response
            ->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLoginWithNoActivationTest()
    {
        $data = [
            'email' => "example" . str_random(3) . "@example.com",
            'name' => 'test',
            'surname' => 'test',
            'birth_date' => '2000-08-06 20:17:29',
        ];

        $this->json('POST', '/api/auth/register', $data + ['password' => '12345678']);


        $response = $this->json('POST', '/api/auth/login', $data + ['password' => '12345678']);

        $response
            ->assertStatus(401)
            ->assertJson(['error' => 'Please activate your account first.']);
    }
}
