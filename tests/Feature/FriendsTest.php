<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FriendsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEmptyFriends()
    {
        $email = "example" . str_random(3) . "@example.com";

        $data = [
            'email' => $email,
            'name' => 'test',
            'surname' => 'test',
            'birth_date' => '2000-08-06 20:17:29',
        ];

        $this->json('POST', '/api/auth/register', $data + ['password' => '12345678']);

        $user = User::where('email', $email)->first();

        $user->activate_token = null;
        $user->save();

        $response = $this
            ->actingAs($user)
            ->json('GET', '/api/friends');

        $response
            ->assertJson([])
            ->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testNotEmptyFriends()
    {
        $email = "example" . str_random(3) . "@example.com";
        $email2 = "example2" . str_random(3) . "@example.com";

        $data = [
            'email' => $email,
            'name' => 'test',
            'surname' => 'test',
            'birth_date' => '2000-08-06 20:17:29',
        ];
        $data2 = [
            'email' => $email2,
            'name' => 'test',
            'surname' => 'test',
            'birth_date' => '2000-08-06 20:17:29',
        ];

        $this->json('POST', '/api/auth/register', $data + ['password' => '12345678']);
        $this->json('POST', '/api/auth/register', $data2 + ['password' => '12345678']);


        $user = User::where('email', $email)->first();
        $user2 = User::where('email', $email2)->first();


        $user->activate_token = null;
        $user->save();
        $user2->activate_token = null;
        $user2->save();

        $invitation = $this
            ->actingAs($user)
            ->json('POST', '/api/friends/invitation', ['user_id_invited' => $user2->id]);

        $invitation = $this
            ->actingAs($user2)
            ->json('GET', '/api/friends/invitation')->decodeResponseJson();

        $invitation = $this
            ->actingAs($user2)
            ->json('PUT', '/api/friends/invitation/' . $invitation[0]['id'] . '/accept')->decodeResponseJson();

        $response = $this
            ->actingAs($user)
            ->json('GET', '/api/friends');

        $response->assertJsonCount(1)
            ->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSearchFriends()
    {
        $email = "example" . str_random(3) . "@example.com";
        $email2 = "example2" . str_random(3) . "@example.com";

        $data = [
            'email' => $email,
            'name' => 'test',
            'surname' => 'test',
            'birth_date' => '2000-08-06 20:17:29',
        ];
        $nameToSearch = 'sadsasad' . str_random(3);

        $data2 = [
            'email' => $email2,
            'name' => $nameToSearch,
            'surname' => 'test',
            'birth_date' => '2000-08-06 20:17:29',
        ];

        $this->json('POST', '/api/auth/register', $data + ['password' => '12345678']);
        $this->json('POST', '/api/auth/register', $data2 + ['password' => '12345678']);


        $user = User::where('email', $email)->first();
        $user2 = User::where('email', $email2)->first();


        $user->activate_token = null;
        $user->save();
        $user2->activate_token = null;
        $user2->save();


        $response = $this
            ->actingAs($user)
            ->json('GET', '/api/friends/search', ['search' => $nameToSearch]);

        $response
            ->assertJsonCount(1)
            ->assertStatus(200);
    }
}
