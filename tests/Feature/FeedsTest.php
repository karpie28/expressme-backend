<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FeedsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPostFeedFriends()
    {
        $email = "example" . str_random(3) . "@example.com";

        $data = [
            'email' => $email,
            'name' => 'test',
            'surname' => 'test',
            'birth_date' => '2000-08-06 20:17:29',
        ];

        $this->json('POST', '/api/auth/register', $data + ['password' => '12345678']);

        $user = User::where('email', $email)->first();
        $user->activate_token = null;
        $user->save();

        $response = $this
            ->actingAs($user)
            ->json('POST', '/api/feeds', [
            'description' => 'asdsadasdasdas',
        ]);

        $response
            ->assertJsonCount(8)
            ->assertStatus(201);
    }

}