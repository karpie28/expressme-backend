<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\File;

/**
 * @SWG\Definition(required={"description"},type="object", @SWG\Xml(name="Feed"))
 */
class FeedComment extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'feed_id',
        'user_id',
        'description',
    ];

    protected $appends = [
        'full_name',
    ];

    public function getFullnameAttribute()
    {
        $user = User::find($this->user_id);

        if (!$user) {
            return null;
        }

        return $user->name . ' ' . $user->surname;
    }

}