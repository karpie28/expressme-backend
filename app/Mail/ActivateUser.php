<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ActivateUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $activateToken;

    public function __construct($activateToken)
    {
        $this->activateToken = $activateToken;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $urlToRedirect = env('EXPRESSME_FRONT') . "/" . "activate/{$this->activateToken}";

        return $this->view('emails.userActivate', ['urlToRedirect' => $urlToRedirect]);
    }
}
