<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @SWG\Definition(required={"user_id", "email", "name"},type="object", @SWG\Xml(name="User"))
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $fillable = [
        'name',
        'email',
        'password',
        'activate_token',
        'surname',
        'birth_date',
        'avatar',
        'last_emotion',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function waitingInvitations()
    {
        return $this->hasMany('App\Invitation', 'user_id_invited');
    }

    /**
     * A user can have many messages
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    /**
     * @SWG\Property(format="int64", property="id", description="The user identifier.")
     * @var int
     */

    /**
     * @SWG\Property(property="email", description="The user email.", example="example@mail.com")
     * @var string
     */

    /**
     * @var string
     * @SWG\Property(property="name")
     */
    /**
     * @var string
     * @SWG\Property(property="surname")
     */
    /**
     * @var string
     * @SWG\Property(property="birth_date")
     */
    /**
     * @var string
     * @SWG\Property(property="avatar")
     */
    /**
     * @var integer
     * @SWG\Property(property="last_emotion")
     */
    /**
     * @var string
     * @SWG\Property(property="created_at")
     */
    /**
     * @var string
     * @SWG\Property(property="updated_at")
     */
}