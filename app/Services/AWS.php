<?php

namespace App\Services;

use Aws\Comprehend\ComprehendClient;
use Aws\Credentials\CredentialProvider;
use Aws\Rekognition\RekognitionClient;
use Aws\S3\S3Client;

class AWS
{
    public function sendImage($image)
    {
        $rekognition = new RekognitionClient([
            'version' => 'latest',
            'region'  => 'us-east-1',
            'credentials' => [
                'key'    => env('SES_KEY'),
                'secret' => env('SES_SECRET'),
            ],

        ]);

        $result = $rekognition->detectFaces([
            'Attributes' => ["ALL"],
            'Image' => [
                'Bytes' => file_get_contents($image),
            ],
            "MaxLabels" => 0,
            'MinConfidence' => 10
        ]);

        return $result;

    }


    public function sendText($text)
    {
        $comprehend = new ComprehendClient([
            'version' => 'latest',
            'region'  => 'us-east-1',
            'credentials' => [
                'key'    => env('SES_KEY'),
                'secret' => env('SES_SECRET'),
            ],

        ]);


        $result = $comprehend->detectSentiment([
            'LanguageCode' => 'en', // REQUIRED
            'Text' => $text, // REQUIRED
        ]);

        return $result;

    }




}