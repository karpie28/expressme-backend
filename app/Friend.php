<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{
	protected $fillable = [
		'user_id_inviting',
		'user_id_invited',
	];
}
