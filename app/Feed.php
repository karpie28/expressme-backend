<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\File;
use Tests\Feature\FeedsTest;

/**
 * @SWG\Definition(required={"description"},type="object", @SWG\Xml(name="Feed"))
 */
class Feed extends Model
{
    protected $primaryKey = 'feed_id';

    protected $fillable = [
        'user_id',
        'description',
        'image',
        'emotion',
        'imageEmotion',
        'textEmotion',
        'width',
        'height',
    ];

    protected $appends = [
        'user',
        'likes_count',
        'comments',
    ];

    public function getUserAttribute()
    {
        $user = User::find($this->user_id);

        if (!$user) {
            return [];
        }

        return $user;
    }

    public function getLikesCountAttribute()
    {
        return FeedLike::where('feed_id', $this->feed_id)
            ->count();
    }

    public function getCommentsAttribute()
    {
        return FeedComment::where('feed_id', $this->feed_id)
            ->get();
    }

    /**
     * @SWG\Property(property="description", description="Feed content", example="Great feed")
     * @var string
     */

    /**
     * @var number
     * @SWG\Property(property="user_id")
     */
    /**
     * @var string
     * @SWG\Property(property="image", description="Absolute URL to image")
     */
    /**
     * /**
     * @var string
     * @SWG\Property(property="User", description="Full user object")
     */
    /**
     * @var number
     * @SWG\Property(property="emotion", description="Overall emotion based on scale 1-5, where 1 means really sad, 5 means really happy.
     *         3 means neutral, but when photo is happy, and text is sad, then result will be neutral")
     */
    /**
     * @var number
     * @SWG\Property(property="imageEmotion", description="Null, when there was no image, or image emotion based on scale 1-5, where 1 means really sad, 5 means really happy.")
     */
    /**
     * @var number
     * @SWG\Property(property="textEmotion", description="textEmotion based on scale 1-5, where 1 means really sad, 5 means really happy.")
     */
    /**
     * /**
     * @var number
     * @SWG\Property(property="height", description="Height of image when present, if not then null.")
     */
    /**
     * @var number
     * @SWG\Property(property="width", description="Height of image when present, if not then null.")
     */
    /**
     * @var string
     * @SWG\Property(property="created_at")
     */
    /**
     * @var string
     * @SWG\Property(property="updated_at")
     */
}