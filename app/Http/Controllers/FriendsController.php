<?php

namespace App\Http\Controllers;

use App\Friend;
use App\FriendPhoto;
use App\Invitation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class FriendsController extends Controller
{

    /**
     * @SWG\Get(
     *   path="/api/friends/search",
     *   summary="Search user by name.",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/friends"},
     *
     * @SWG\Parameter(
     *        in="header",
     *        name="Authorization",
     *        required=true,
     *        type="string",
     *        description="Authorization key must have 'Bearer ' prefix.",
     *        ),
     *   @SWG\Parameter(
     *     name="search",
     *     in="formData",
     *     description="String of searching value",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Users with given string.",
     *     @SWG\Items(ref="#/definitions/User")
     *   )
     * )
     */
    public function searchFriends(Request $request)
    {
        $search = $request->input('search');

        $invitations = Friend::where('user_id_invited', Auth::user()->id)
            ->get()->pluck('user_id_inviting')->toArray();

        array_push($invitations, Auth::user()->id);

        $users = User::where(function ($q) use ($search) {
            $q->where('name', 'like', "%{$search}%")
                ->orWhere('surname', 'like', "%{$search}%")
                ->orWhere('email', 'like', "%{$search}%");
        })->whereNotIn('id', $invitations)
            ->get();

        return $users;
    }


    /**
     * @SWG\Get(
     *   path="/api/friends/invitation",
     *   summary="Get all invitations that are sent to you",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/friends"},
     *
     *   @SWG\Response(
     *     response=200,
     *     description="Succesfull operation. Invitation accepted, Friend Added.",
     *   )
     * )
     */
    public function listAllWaitingInvites()
    {
        return Auth::user()->waitingInvitations()->get();
    }

    /**
     * @SWG\Post(
     *   path="/api/friends/invitation",
     *   summary="Send invitation to given Id.",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/friends"},
     *   @SWG\Parameter(
     *        in="header",
     *        name="Authorization",
     *        required=true,
     *        type="string",
     *        description="Authorization key must have 'Bearer ' prefix.",
     *        ),
     *   @SWG\Parameter(
     *     name="user_id_invited",
     *     in="formData",
     *     description="Id of user you want to invite.",
     *     required=true,
     *     type="number"
     *   ),
     *   @SWG\Response(
     *     response=400,
     *     description="There was a problem with this invitation",
     *   ),
     *   @SWG\Response(
     *     response=401,
     *     description="Unauthorized",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Succesfull operation. Invitation accepted, Friend Added.",
     *   )
     * )
     */

    public function sendInvite(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id_invited' => 'required|exists:users,id',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        $user_id_invited = $request->input('user_id_invited');
        $user_id = Auth::user()->id;

        $invitation = Invitation::where('user_id_invited', $user_id)
            ->where('user_id_inviting', $user_id_invited)
            ->where('status', '!=', 'accepted')
            ->exists();

        if ($invitation) {
            return response()->json(['error' => 'You are already friends.'], 400);
        }

        if ($user_id_invited == $user_id) {
            return response()->json(['error' => 'You can\'t invite yourself :)'], 400);
        }

        $invitation = Invitation::where('user_id_inviting', $user_id)->where('user_id_invited', $user_id_invited)->first();

        if (isset($invitation)) {
            if ($invitation->status == 'blocked') {
                return response()->json(['error' => 'This user has already blocked you.'], 400);
            } else {
                return response()->json(['error' => 'You have already invited that user. Please wait for his accept.'], 400);
            }
        }

        Invitation::create([
            'user_id_inviting' => $user_id,
            'user_id_invited' => $request->input('user_id_invited'),
            'status' => 'waiting'
        ]);

        return response()->json(['message' => 'Your invite has been sent.'], 200);
    }

    /**
     * @SWG\Put(
     *   path="/api/friends/invitation/{invitationId}/accept",
     *   summary="Accept invitation with given Id.",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/friends"},
     *
     *
     *   @SWG\Parameter(
     *        in="header",
     *        name="Authorization",
     *        required=true,
     *        type="string",
     *        description="Authorization key must have 'Bearer ' prefix.",
     *        ),
     *   @SWG\Response(
     *     response=404,
     *     description="There is no such invitation",
     *   ),
     *   @SWG\Response(
     *     response=400,
     *     description="There was a problem with this invitation",
     *   ),
     *   @SWG\Response(
     *     response=401,
     *     description="Unauthorized",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Succesfull operation. Invitation accepted, Friend Added.",
     *   )
     * )
     */

    public function acceptInvite($invitationId)
    {
        $userId = Auth::user()->id;
        $invitation = Invitation::where('user_id_invited', $userId)->where('id', $invitationId)->first();

        if (!$invitation) {
            return response()->json(['error' => 'There is no such invitation.'], 404);
        }

        if ($invitation->status != 'waiting') {
            return response()->json(['error' => 'This invitation has already been ' . $invitation->status], 400);
        }

        $invitation->status = 'accepted';
        $invitation->save();

        Friend::create([
            'user_id_inviting' => $invitation->user_id_inviting,
            'user_id_invited' => $userId,
        ]);

        Friend::create([
            'user_id_inviting' => $userId,
            'user_id_invited' => $invitation->user_id_inviting,
        ]);

        return response()->json(['message' => 'Invitation has been accepted.'], 200);
    }

    /**
     * @SWG\Put(
     *   path="/api/friends/invitation/{invitationId}/reject",
     *   summary="Reject invitation with given Id.",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/friends"},
     *   @SWG\Parameter(
     *        in="header",
     *        name="Authorization",
     *        required=true,
     *        type="string",
     *        description="Authorization key must have 'Bearer ' prefix.",
     *        ),
     *   @SWG\Response(
     *     response=404,
     *     description="There is no such invitation",
     *   ),
     *   @SWG\Response(
     *     response=400,
     *     description="There was a problem with this invitation",
     *   ),
     *   @SWG\Response(
     *     response=401,
     *     description="Unauthorized",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Succesfull operation.",
     *   )
     * )
     */

    public function rejectInvite($invitationId)
    {
        $userId = Auth::user()->id;
        $invitation = Invitation::where('user_id_invited', $userId)->where('id', $invitationId)->first();

        if (!$invitation) {
            return response()->json(['error' => 'There is no such invitation.'], 404);
        }

        if ($invitation->status != 'waiting') {
            return response()->json(['error' => 'This invitation has already been ' . $invitation->status], 400);
        }

        $invitation->status = 'rejected';
        $invitation->save();

        return response()->json(['error' => 'Invitation has been rejected.'], 200);
    }

    /**
     * @SWG\Put(
     *   path="/api/friends/invitation/{invitationId}/block",
     *   summary="Block invitation with given Id.",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/friends"},
     *   @SWG\Parameter(
     *        in="header",
     *        name="Authorization",
     *        required=true,
     *        type="string",
     *        description="Authorization key must have 'Bearer ' prefix.",
     *        ),
     *   @SWG\Response(
     *     response=404,
     *     description="There is no such invitation",
     *   ),
     *   @SWG\Response(
     *     response=400,
     *     description="There was a problem with this invitation",
     *   ),
     *   @SWG\Response(
     *     response=401,
     *     description="Unauthorized",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Succesfull operation.",
     *   )
     * )
     */

    public function blockInvite($invitationId)
    {
        $userId = Auth::user()->id;
        $invitation = Invitation::where('user_id_invited', $userId)->where('id', $invitationId)->first();

        if (!$invitation) {
            return response()->json(['error' => 'There is no such invitation.'], 404);
        }

        if ($invitation->status != 'waiting') {
            return response()->json(['error' => 'This invitation has already been ' . $invitation->status], 400);
        }

        $invitation->status = 'blocked';
        $invitation->save();

        return response()->json(['message' => 'Invitation has been blocked.'], 200);
    }

    /**
     * @SWG\Delete(
     *   path="/api/friends/{friendId]",
     *   summary="Delete friend with given Id.",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/friends"},
     *   @SWG\Parameter(
     *        in="header",
     *        name="Authorization",
     *        required=true,
     *        type="string",
     *        description="Authorization key must have 'Bearer ' prefix.",
     *        ),
     *   @SWG\Response(
     *     response=404,
     *     description="There is no such friend",
     *   ),
     *   @SWG\Response(
     *     response=401,
     *     description="Unauthorized",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Succesfull operation.",
     *   )
     * )
     */
    public function removeFriend($friendId)
    {
        $userId = Auth::user()->id;
        $friends = Friend::where('user_id_invited', $userId)->where('user_id_inviting', $friendId)->first();
        $friends2 = Friend::where('user_id_invited', $friendId)->where('user_id_inviting', $userId)->first();


        Invitation::where('user_id_invited', $userId)->where('user_id_inviting', $friendId)->delete();
        Invitation::where('user_id_invited', $friendId)->where('user_id_inviting', $userId)->delete();
        $yes = false;

        if (isset($friends)) {
            $friends->delete();
            $yes = true;
        }

        if (isset($friends2)) {
            $friends2->delete();
            $yes = true;
        }

        if ($yes) {
            return response()->json(['message' => 'Friend has been removed.'], 200);

        }

        return response()->json(['error' => 'Friend has not been found.'], 404);
    }

    /**
     * @SWG\Get(
     *   path="/api/friends",
     *   summary="Get all friends",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/friends"},
     *   @SWG\Parameter(
     *        in="header",
     *        name="Authorization",
     *        required=true,
     *        type="string",
     *        description="Authorization key must have 'Bearer ' prefix.",
     *        ),
     *   @SWG\Response(
     *     response=401,
     *     description="Unauthorized",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Succesfull operation.",
     *      @SWG\Items(ref="#/definitions/User")
     *   )
     * )
     */

    public function listAllFriends()
    {
        $userId = Auth::user()->id;


//        $friends = DB::select("SELECT users.id, users.name, users.surname, users.birth_date, users.email FROM friends
//INNER JOIN users ON ((friends.user_id_inviting = users.id AND friends.user_id_invited={$userId})
//or (friends.user_id_invited = users.id AND friends.user_id_inviting={$userId}))
//");

        $friends = User::leftJoin('friends', 'friends.user_id_invited', '=', 'users.id')
            ->where('friends.user_id_inviting', $userId)
            ->select('users.*')
            ->get();

        return $friends;
    }


    /**
     * @SWG\Get(
     *   path="/api/friends/{friendId]",
     *   summary="Get friend with given Id.",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/friends"},
     *   @SWG\Parameter(
     *        in="header",
     *        name="Authorization",
     *        required=true,
     *        type="string",
     *        description="Authorization key must have 'Bearer ' prefix.",
     *        ),
     *   @SWG\Response(
     *     response=404,
     *     description="There is no such friend",
     *   ),
     *   @SWG\Response(
     *     response=401,
     *     description="Unauthorized",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Succesfull operation.",
     *      @SWG\Items(ref="#/definitions/User")
     *   )
     * )
     */

    public function getFriend($friendId)
    {
        $userId = Auth::user()->id;


        $friends = DB::select("SELECT users.id, users.name, users.surname, users.birth_date, users.email FROM friends 
INNER JOIN users ON ((friends.user_id_inviting = users.id AND friends.user_id_invited={$userId})
or (friends.user_id_invited = users.id AND friends.user_id_inviting={$userId})) WHERE users.id = ?
", [$friendId]);

        if (count($friends) === 0) {
            return response()->json(['error' => 'There is no such friend'], 404);
        }

        return $friends;
    }

}
