<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * @SWG\Swagger(
 *     schemes={"https"},
 *     host="api.expressme.pl",
 *     basePath="/",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="This is documentation of expressme.pl api",
 *         description="Remember to use only HTTPS!",
 *         @SWG\Contact(
 *             email="contact@expressme.pl"
 *         ),
 *     ),
 * )
 */

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
