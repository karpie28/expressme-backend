<?php

namespace App\Http\Controllers;

use App\Feed;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Google\Cloud\Translate\TranslateClient;
use App\FeedComment;
use App\FeedLike;

class FeedsController extends Controller
{

    /**
     * @SWG\Get(
     *   path="/api/feeds",
     *   summary="Get all user feeds",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/feeds"},
     *   @SWG\Parameter(
     *        in="header",
     *        name="Authorization",
     *        required=true,
     *        type="string",
     *        description="Authorization key must have 'Bearer ' prefix.",
     *        ),
     *   @SWG\Parameter(
     *        in="query",
     *        name="limit",
     *        type="integer",
     *        ),
     *     @SWG\Parameter(
     *        in="query",
     *        name="offset",
     *        type="integer",
     *        ),
     *   @SWG\Response(
     *     response=200,
     *     description="Feeds.",
     *     @SWG\Items(ref="#/definitions/Feed")
     *   ),
     *   @SWG\Response(
     *     response=400,
     *     description="Bad validator."
     *   )
     * )
     */
    public function getMeFeeds(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'limit' => 'numeric',
            'offset' => 'numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $limit = $request->input('limit', 10);
        $offset = $request->input('offset', 0);

        $userId = Auth::user()->id;

        $feeds = Feed::select('feeds.*')
            ->leftJoin('friends', 'friends.user_id_invited', '=', 'feeds.user_id')
            ->where('friends.user_id_inviting', $userId)
            ->orWhere('feeds.user_id', $userId)
            ->orderBy('created_at', 'desc')
            ->limit($limit)->offset($offset)
            ->get();

        return $feeds;
    }

    /**
     * @SWG\Post(
     *   path="/api/feeds",
     *   summary="Post feed",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/feeds"},
     *
     * @SWG\Parameter(
     *        in="header",
     *        name="Authorization",
     *        required=true,
     *        type="string",
     *        description="Authorization key must have 'Bearer ' prefix.",
     *        ),
     *  @SWG\Parameter(
     *     name="description",
     *     in="formData",
     *     description="TExt of feed",
     *     required=true,
     *     type="string"
     *   ),
     *  @SWG\Parameter(
     *     name="image",
     *     in="formData",
     *     description="File as image. Only jpg, jpeg, png. Max size: 3 MB",
     *     required=true,
     *     type="file"
     *   ),
     *   @SWG\Response(
     *     response=201,
     *     description="Feeds.",
     *     @SWG\Items(ref="#/definitions/Feed")
     *   )
     * )
     */
    public function postFeed(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'image|mimes:jpg,jpeg,png|max:3000',
            'description' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $userId = Auth::user()->id;

        $description = $request->get('description');

        $textPoints = $this->getTextEmotions($description);

        $image = $request->file('image');

        $pathToSave = null;

        $width = null;
        $height = null;

        if ($request->has('image')) {
            $pathToSave = Storage::disk('s3')->put( 'users/' . $userId . '/feeds', $image, 'public');
            $pathToSave = Storage::cloud()->url($pathToSave);
           $imagePoints = $this->getImageEmotions($image);

            $data = getimagesize($image);
            $width = $data[0];
            $height = $data[1];
        }

        $emotionPoints = isset($imagePoints) ? ($textPoints + $imagePoints) / 2 : $textPoints;

        $feed = Feed::create([
            'user_id' => $userId,
            'description' => $description,
            'image' => $pathToSave,
            'emotion' => $emotionPoints,
            'textEmotion' => $textPoints,
            'width' => $width,
            'height' => $height,
        ]);

        $user = Auth::user();
        $user->last_emotion = $emotionPoints;
        $user->save();

        if(isset($imagePoints)) {
            $feed->imageEmotion = $imagePoints;
            $feed->save();
        }

        return $feed;
    }

    public function getTextEmotions($description)
    {
        if(env('APP_ENV') === 'local') {
            putenv('GOOGLE_APPLICATION_CREDENTIALS=/Users/karol/projects/expressme-backend/app/Http/Controllers/expressme-6e3636340bc5.json');
        } else {
            putenv('GOOGLE_APPLICATION_CREDENTIALS=/var/www/vhosts/expressme.pl/api.expressme.pl/app/Http/Controllers/expressme-6e3636340bc5.json');
        }


        $translation = (new TranslateClient([
            'projectId' => '545730327884'
        ]))->translate($description, [
            'target' => 'en'
        ]);

        $sentiment = (new \App\Services\AWS())->sendText($translation['text'])->get('Sentiment');

        $textPoints = 0;

        switch ($sentiment) {
            case 'NEGATIVE':
                $textPoints = 1;
                break;
            case 'NEUTRAL':
                $textPoints = 3;
                break;
            case 'POSITIVE':
                $textPoints = 5;
                break;
        }

        return $textPoints;
    }

    public function getImageEmotions($image)
    {
        $awsResponse = (new \App\Services\AWS())->sendImage($image);
        $emotions = $awsResponse->get('FaceDetails');


        if (!isset($emotions[0]['Emotions'][0]["Type"])) {
            return 0;
        }
        $emotions = $emotions[0]['Emotions'][0]["Type"];
        $photoPoints = 0;

        switch ($emotions) {
            case 'HAPPY':
                $photoPoints = 5;
                break;
            case 'SAD':
                $photoPoints = 1;
                break;
            case 'ANGRY':
                $photoPoints = 2;
                break;
            case 'CONFUSED':
                $photoPoints = 3;
                break;
            case 'DISGUSTED':
                $photoPoints = 1;
                break;
            case 'SURPRISED':
                $photoPoints = 3;
                break;
            case 'CALM':
                $photoPoints = 3;
                break;
        }

        return $photoPoints;
    }

    /**
     * @SWG\Post(
     *   path="/api/feeds/{feed_id}/like",
     *   summary="Like feed",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/feeds"},
     *
     * @SWG\Parameter(
     *        in="header",
     *        name="Authorization",
     *        required=true,
     *        type="string",
     *        description="Authorization key must have 'Bearer ' prefix.",
     *        ),
     *  @SWG\Parameter(
     *     name="feed_id",
     *     in="path",
     *     description="Feed id",
     *     required=true,
     *     type="number"
     *   ),

     *   @SWG\Response(
     *     response=200,
     *     description="Feeds.",
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="No such feed correlated with your friends.",
     *   )
     * )
     */
    public function likeFeed($feedId)
    {
        $userId = Auth::user()->id;

        $feed = Feed::where('feed_id', $feedId)
            ->first();

        if(!$feed) {
            return response()->json(['error' => 'There is no such feed.'], 404);
        }

        $feedLike = FeedLike::where('feed_id', $feed->feed_id)
            ->where('user_id', $feed->user_id)
            ->first();

        if($feedLike) {
            return response()->json(['error' => 'Feed already liked.'], 400);
        }


        FeedLike::create([
            'feed_id' => $feed->feed_id,
            'user_id' => $userId
        ]);

        return response()->json(['message' => 'Feed liked.'], 200);
    }

    /**
     * @SWG\Get(
     *   path="/api/feeds/{feed_id}/like",
     *   summary="Get likes of feed",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/feeds"},
     *
     * @SWG\Parameter(
     *        in="header",
     *        name="Authorization",
     *        required=true,
     *        type="string",
     *        description="Authorization key must have 'Bearer ' prefix.",
     *        ),
     *  @SWG\Parameter(
     *     name="feed_id",
     *     in="path",
     *     description="Feed id",
     *     required=true,
     *     type="number"
     *   ),

     *   @SWG\Response(
     *     response=200,
     *     description="Feeds.",
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="No such feed correlated with your friends.",
     *   )
     * )
     */
    public function likeFeedList($feedId)
    {
        $feed = Feed::where('feed_id', $feedId)
            ->first();

        if(!$feed) {
            return response()->json(['error' => 'There is no such feed.'], 404);
        }

        $feedLikes = FeedLike::where([
            'feed_id' => $feed->feed_id,
        ])->get();

        foreach ($feedLikes as $feedLike) {
            $user = User::where('id', $feedLike->user_id)->first();
            $feedLike->full_name = $user->name . ' ' . $user->surname;
        }

        return $feedLikes;
    }

    /**
     * @SWG\Post(
     *   path="/api/feeds/{feed_id}/comment",
     *   summary="Comment feed",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/feeds"},
     *
     * @SWG\Parameter(
     *        in="header",
     *        name="Authorization",
     *        required=true,
     *        type="string",
     *        description="Authorization key must have 'Bearer ' prefix.",
     *        ),
     *  @SWG\Parameter(
     *     name="Feed id",
     *     in="path",
     *     description="Feed id",
     *     required=true,
     *     type="number"
     *   ),
     *  @SWG\Parameter(
     *     name="description",
     *     in="path",
     *     description="description",
     *     required=true,
     *     type="number"
     *   ),

     *   @SWG\Response(
     *     response=200,
     *     description="Feeds.",
     *   ),
     *   @SWG\Response(
     *     response=400,
     *     description="Validation Error",
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="No such feed correlated with your friends.",
     *   )
     * )
     */
    public function addFeedComment($feedId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $userId = Auth::user()->id;

        $feed = Feed::where('feed_id', $feedId)
            ->first();

        if(!$feed) {
            return response()->json(['error' => 'There is no such feed.'], 404);
        }

        FeedComment::create([
            'feed_id' => $feed->feed_id,
            'user_id' => $userId,
            'description' => $request->input('description'),
        ]);

        return response()->json(['message' => 'Feed commented.'], 200);
    }



}
