<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Events\NewMessage;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Pusher\Pusher;

class ChatsController extends Controller
{

    protected $pusher;

    public function __construct()
    {
        $this->pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'));
    }


    /**
     * @SWG\Get(
     *   path="/api/chat/messages/{userIdFrom}",
     *   summary="Get all user feeds",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/chat"},
     *   @SWG\Parameter(
     *        in="header",
     *        name="Authorization",
     *        required=true,
     *        type="string",
     *        description="Authorization key must have 'Bearer ' prefix.",
     *        ),
     *   @SWG\Parameter(
     *        in="query",
     *        name="limit",
     *        type="integer",
     *        ),
     *     @SWG\Parameter(
     *        in="query",
     *        name="offset",
     *        type="integer",
     *        ),
     *   @SWG\Response(
     *     response=200,
     *     description="Messages.",
     *     @SWG\Items(ref="#/definitions/Message")
     *   ),
     *   @SWG\Response(
     *     response=400,
     *     description="Bad validator."
     *   )
     * )
     */
    public function fetchMessages($userIdFrom, Request $request)
    {

        $validator = Validator::make($request->all(), [
            'limit' => 'numeric',
            'offset' => 'numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $limit = $request->input('limit', 10);
        $offset = $request->input('offset', 0);

        $userId = Auth::user()->id;
        $first = $userId;


        $second = $userIdFrom;

        if ($first > $second) {
            $help = $first;
            $first = $second;
            $second = $help;
        }

        $messages = Message::where(function ($q) use ($first, $second) {
            $q->where('user_id', $first)
                ->where('user_sent_id', $second);
        })->orWhere(function ($q) use ($first, $second) {
            $q->where('user_id', $second)
                ->where('user_sent_id', $first);
        })
        ->orderBy('id', 'desc')
        ->skip($offset)
            ->take($limit)
            ->get()->toArray();

        $messages = array_reverse($messages);

        return $messages;
    }

    /**
     * @SWG\Post(
     *   path="/api/chat/messages/{userIdToSend}",
     *   summary="Post message to User",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/chat"},
     *
     * @SWG\Parameter(
     *        in="header",
     *        name="Authorization",
     *        required=true,
     *        type="string",
     *        description="Authorization key must have 'Bearer ' prefix.",
     *        ),
     *  @SWG\Parameter(
     *     name="message",
     *     in="formData",
     *     description="TExt of message",
     *     required=true,
     *     type="string"
     *   ),
     *  @SWG\Parameter(
     *     name="image",
     *     in="formData",
     *     description="File as image. Only jpg, jpeg, png. Max size: 3 MB",
     *     type="file"
     *   ),
     *   @SWG\Response(
     *     response=201,
     *     description="status."
     *   )
     * )
     */
    public function sendMessage(Request $request, $userIdToSent)
    {

        $validator = Validator::make($request->all(), [
            'image' => 'image|mimes:jpg,jpeg,png|max:3000',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $userToSent = User::find($userIdToSent);

        if (!$userToSent) {
            return response()->json(['error' => 'There is no such user.'], 404);
        }

        $user = Auth::user();

        if ($user->id == $userIdToSent) {
            return response()->json(['error' => 'You can\' send messages for yourself'], 400);
        }

        $first = $userIdToSent;

        $second = $user->id;

        if ($first > $second) {
            $help = $first;
            $first = $second;
            $second = $help;
        }

        $chatName = 'private-chat' . '-' . $first . '-' . $second;

        $feeds = new FeedsController();
        $textPoints = $feeds->getTextEmotions($request->input('message', ''));

        $pathToSave = null;

        if ($request->has('image')) {
            $image = $request->file('image');

            $pathToSave = Storage::disk('s3')->put('users/' . $user->id . '/messages', $image, 'public');
            $pathToSave = Storage::cloud()->url($pathToSave);
            $imagePoints = $feeds->getImageEmotions($image);
        }

        $emotionPoints = isset($imagePoints) ? ($textPoints + $imagePoints) / 2 : $textPoints;

        $message = Message::create([
            'user_id' => $user->id,
            'text' => $request->input('message', ''),
            'user_sent_id' => $userIdToSent,
            'image' => $pathToSave,
            'emotion' => $emotionPoints,
            'textEmotion' => $textPoints,
        ]);

        if (isset($imagePoints)) {
            $message->imageEmotion = $imagePoints;
            $message->save();
        }

        broadcast(new NewMessage($user, $userToSent, $chatName))->toOthers();

        broadcast(new NewMessage($userToSent, $user, $chatName))->toOthers();

        broadcast(new MessageSent($user, $message, $userIdToSent))->toOthers();

        return ['status' => 'Message Sent!'];
    }

    public function authorizeChat(Request $request)
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return $this->pusher->socket_auth(
                $request->input('channel_name'),
                $request->input('socket_id')
            );
        }

        return response()->json(['error' => 'Unauthorized'], 403);
    }
}
