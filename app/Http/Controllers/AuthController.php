<?php

namespace App\Http\Controllers;

use App\Mail\ActivateUser;
use App\Mail\ResetPassword;
use App\PasswordReset;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    /**
     * @SWG\Post(
     *   path="/api/auth/login",
     *   summary="Login user.",
     *   description="This method logins user and returns his data.
     *      access_token default has 12 hours lifetime.
     *     To keep signed in after that time, please POST at api/auth/refresh",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/auth"},
     *
     *     @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="The user email for login",
     *     required=true,
     *     type="string"
     *   ),
     *     @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="Password of user",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=400,
     *     description="Something went wrong",
     *   ),
     *   @SWG\Response(
     *     response=401,
     *     description="Unauthorized",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Succesfull operation.",
     *      @SWG\Items(ref="#/definitions/User")
     *   )
     * )
     */
    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Email or password do not match'], 401);
        }

        if (!is_null(auth()->user()->activate_token)) {
            return response()->json(['error' => 'Please activate your account first.'], 401);
        }

        return $this->respondWithToken($token);
    }


    /**
     * @SWG\Post(
     *   path="/api/auth/register",
     *   summary="Register user.",
     *   description="This method registers user and returns his data.",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/auth"},
     *
     *     @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="The user email for login",
     *     required=true,
     *     type="string"
     *   ),
     *     @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="Password of user",
     *     required=true,
     *     type="string"
     *   ),
     *     @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="Name of user",
     *     required=true,
     *     type="string"
     *   ),
     *     @SWG\Parameter(
     *     name="surname",
     *     in="formData",
     *     description="Surname of user",
     *     required=true,
     *     type="string"
     *   ),
     *     @SWG\Parameter(
     *     name="birth_date",
     *     in="formData",
     *     description="BirthDate. example: 1996-08-19 00:00:00",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=401,
     *     description="Unauthorized",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Succesfull operation.",
     *      @SWG\Items(ref="#/definitions/User")
     *   )
     * )
     */

    protected function create(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'birth_date' => 'required|date_format:Y-m-d H:i:s',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);;

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $userExists = User::where('email', $request->get('email'))->exists();
        if($userExists) {
            return response()->json(['error' => 'There is already such email registered'], 400);
        }

        $date = (new Carbon($request->get('birth_date')))->addYears(14);

        $date2 = (new Carbon($request->get('birth_date')))->subYears(130);

        if ($date2->gt(new Carbon('1900-01-14 20:17:21'))) {
            return response()->json(['error' => 'Youcan\'t be that old'], 400);
        }

        if ($date->gt(Carbon::now())) {
            return response()->json(['error' => 'You must be at least 14 years old'], 400);
        }

        $activateToken = str_random(60);

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'surname' => $request->get('surname'),
            'birth_date' => $request->get('birth_date'),
            'password' => bcrypt($request->password),
            'activate_token' => $activateToken,
        ]);

        Mail::to($request->email)->send(new ActivateUser($activateToken));

        return $user->makeHidden('activate_token');
    }

    /**
     * @SWG\Get(
     *   path="/api/me",
     *   summary="Get the authenticated User.",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api"},
     *
     *   @SWG\Response(
     *     response=200,
     *     description="Succesfull operation.",
     *      @SWG\Items(ref="#/definitions/User")
     *   )
     * )
     *
     *
     **/

    public function me()
    {
        return response()->json(auth()->user()->makeHidden('activate_token'));
    }

    /**
     * @SWG\Post(
     *   path="/api/auth/logout",
     *   summary="Logout user.",
     *   description="Log the user out (Invalidate the token).",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/auth"},
     *    @SWG\Parameter(
     *        in="header",
     *        name="Authorization",
     *        required=true,
     *        type="string",
     *        description="Authorization key must have 'Bearer ' prefix.",
     *        ),
     *   @SWG\Response(
     *     response=200,
     *     description="Successfully logged out",
     *   )
     * )
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */

    /**
     * @SWG\Post(
     *   path="/api/auth/refresh",
     *   summary="Refresh a token.",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/auth"},
     *    @SWG\Parameter(
     *        in="header",
     *        name="Authorization",
     *        required=true,
     *        type="string",
     *        description="Authorization key must have 'Bearer ' prefix.",
     *        ),
     *   @SWG\Response(
     *     response=200,
     *     description="Successful operation.",
     *   )
     * )
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }


    /**
     * @SWG\Get(
     *   path="/api/auth/activate/{activate_token}",
     *   summary="Authenticate the user.",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/auth"},
     *
     *   @SWG\Response(
     *     response=404,
     *     description="User not found",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="User activated.",
     *   )
     * )
     *
     **/

    public function activateAccount($activateToken)
    {
        $user = User::where('activate_token', 'like', $activateToken)->first();

        if (!$user) {
            return response()->json(['message' => 'User has already been activated, or there is no such activation code.'], 404);
        }

        $user->activate_token = null;
        $user->save();

        return response()->json(['message' => 'User has been activated.']);
    }


    /**
     * @SWG\Post(
     *   path="/api/auth/activate/resend",
     *   summary="Resend authentcation code.",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/auth"},
     *
     *     @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="The user email",
     *     required=true,
     *     type="string"
     *   ),
     *
     *   @SWG\Response(
     *     response=404,
     *     description="User not found",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Email sent.",
     *   )
     * )
     *
     **/
    public function resendActivateToken(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $user = User::where('email', $request->email)->whereNotNull('activate_token')->first();

        if (!$user) {
            return response()->json(['message' => 'User has already been activated, or there is no such user.'], 404);
        }

        Mail::to($user->email)->send(new ActivateUser($user->activate_token));

        return response()->json(['message' => 'E-mail has been sent.']);
    }

    /**
     * @SWG\Post(
     *   path="/api/auth/password/reset",
     *   summary="Get reset password.",
     *   description="This method sends email with reset password link.",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/auth"},
     *
     *     @SWG\Parameter(
     *     in="formData",
     *     name="email",
     *     description="",
     *     required=true,
     *     type="string",
     *     description="Email adress for reset password.",
     *     ),
     *     @SWG\Response(
     *     response=400,
     *     description="User not found.",
     *     @SWG\Items(format="string", example="User not found")
     *     ),
     *     @SWG\Response(
     *     response=200,
     *     description="Succesfull operation.",
     *     @SWG\Items(format="string", example="Email sent to reset password")
     *     )
     *    )
     */
    public function passwordReset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $user = User::where('email', $request->input('email'))->first();

        if (!$user) {
            return response()->json(['message' => 'User not found'], 400);

        }
        $token = str_random(60);

        $passwordReset = PasswordReset::where('email', $user->email)->first();

        if ($passwordReset) {
            $passwordReset->delete();
        }
        $passwordReset = new PasswordReset();
        $passwordReset->email = $request->input('email');
        $passwordReset->token = $token;
        $passwordReset->save();

        Mail::to($request->input('email'))->send(
            new ResetPassword($token, $request->input('email')));

        return response()->json(['message' => 'The email to reset password has been sent.'], 200);
    }


    /**
     * @SWG\Post(
     *   path="/api/auth/password/change/{token}",
     *   summary="Post reset password.",
     *   description="This method resets user password.",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api/auth"},
     *
     *      @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="The user password",
     *     required=true,
     *     type="string"
     *   ),
     *     @SWG\Response(
     *     response=400,
     *     description="Token not found.",
     *     @SWG\Items(format="string", example="Token not found")
     *     ),
     *     @SWG\Response(
     *     response=200,
     *     description="Succesfull operation.",
     *     @SWG\Items(format="string", example="Email sent to reset password")
     *     )
     *    )
     */

    public function passwordChange(Request $request, $token)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $passwordReset = PasswordReset::where('token', $token)->first();

        if (!$passwordReset) {
            return response()->json(['message' => 'Token not found'], 404);
        }
        $user = User::where('email', $passwordReset->email)->first();

        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        }

        $user->password = Hash::make($request->input('password'));
        $user->save();

        $passwordReset->delete();

        return response()->json(['message' => 'Password changed.'], 200);
    }

    /**
     * @SWG\Post(
     *   path="/api/me",
     *   summary="Edit user profile.",
     *   description="This method updates user profile.",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *     tags={"/api"},
     *
     *   @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="The user name",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="surname",
     *     in="formData",
     *     description="The user surname",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="avatar",
     *     in="formData",
     *     description="The user avatar",
     *     type="file"
     *   ),
     *     @SWG\Response(
     *     response=400,
     *     description="Validation error.",
     *     @SWG\Items(format="string", example="Validation error.")
     *     ),
     *    @SWG\Response(
     *     response=200,
     *     description="Succesfull operation.",
     *      @SWG\Items(ref="#/definitions/User")
     *   )
     *    )
     */

    public function updateMe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'surname' => 'required',
            'avatar' => 'image|mimes:jpg,jpeg,png|max:3000',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $user = Auth::user();

        $user->name = $request->input('name');
        $user->surname = $request->input('surname');

        $image = $request->file('avatar');

        if ($request->has('avatar')) {
            $pathToSave = Storage::disk('s3')->put('users/' . $user->user_id . 'avatar', $image, 'public');
            $pathToSave = Storage::cloud()->url($pathToSave);

            $imageEmotion = (new FeedsController())->getImageEmotions($image);

            if($imageEmotion != 0) {
                $user->last_emotion = $imageEmotion;
            }

            $user->avatar = $pathToSave;
        }

        $user->save();

        return $user;
    }

}