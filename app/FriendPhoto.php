<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FriendPhoto extends Model
{
    protected $table = 'friends_photos';

    protected $hidden = [
        'photo',
    ];

	protected $fillable = [
		'friend_id',
		'photo',
		'emotions',
	];

    protected $appends = [
        'uploaded_photo'
    ];

    public function getUploadedPhotoAttribute()
    {
        return env('APP_URL') . '/' . $this->photo;
    }
}
