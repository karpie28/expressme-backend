<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/**
 * @SWG\Definition(required={"description"},type="object", @SWG\Xml(name="Message"))
 */
class Message extends Model
{
    protected $fillable = [
        'user_id',
        'user_sent_id',
        'text',
        'image',
        'imageEmotion',
        'emotion',
        'textEmotion',
    ];

    /**
     * A message belong to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * @var number
     * @SWG\Property(property="user_id")
     */
    /**
     * @var string
     * @SWG\Property(property="user_sent_id", description="Bigger id of user chat")
     */
    /**
     * @var string
     * @SWG\Property(property="text", description="Message text")
     */
    /**
     * @var number
     * @SWG\Property(property="emotion", description="Overall emotion based on scale 1-5, where 1 means really sad, 5 means really happy.
     *         3 means neutral, but when photo is happy, and text is sad, then result will be neutral")
     */
    /**
     * @var number
     * @SWG\Property(property="imageEmotion", description="Null, when there was no image, or image emotion based on scale 1-5, where 1 means really sad, 5 means really happy.")
     */
    /**
     * @var number
     * @SWG\Property(property="textEmotion", description="textEmotion based on scale 1-5, where 1 means really sad, 5 means really happy.")
     */
    /**
     * @var string
     * @SWG\Property(property="created_at")
     */
    /**
     * @var string
     * @SWG\Property(property="updated_at")
     */
}
