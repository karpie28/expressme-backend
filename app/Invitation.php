<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    protected $dates = ['deleted_at'];

    protected $hidden = ['deleted_at'];

    protected $fillable = [
		'user_id_inviting',
		'user_id_invited',
		'status',
	];

    protected $appends = [
        'user',
    ];

    public function getUserAttribute()
    {
        $user = User::find($this->user_id_inviting);

        if (!$user) {
            return [];
        }

        return $user;
    }

}
