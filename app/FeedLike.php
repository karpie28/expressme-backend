<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\File;

/**
 * @SWG\Definition(required={"description"},type="object", @SWG\Xml(name="Feed"))
 */
class FeedLike extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'feed_id',
        'user_id',
    ];

}