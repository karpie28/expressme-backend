<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class AddToMessagesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::table('messages', function ($table) {
            $table->tinyInteger('emotion');
            $table->tinyInteger('imageEmotion')->nullable();
            $table->tinyInteger('textEmotion');
            $table->integer('user_sent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages', function ($table) {
            $table->dropColumn('user_sent_id');
            $table->dropColumn('imageEmotion');
            $table->dropColumn('textEmotion');
            $table->dropColumn('image');
        });
    }
}
