<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFriendsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::dropIfExists('friends');
        Schema::dropIfExists('friends_photos');


        Schema::create('friends', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id_inviting')->unsigned();
            $table->integer('user_id_invited')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id_inviting')
                ->references('id')->on('users');
            $table->foreign('user_id_invited')
                ->references('id')->on('users');
        });



    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('friends');

        Schema::create('friends', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name', 255);
            $table->string('surname',255);
            $table->text('description');
            $table->timestamps();
        });

        Schema::create('friends_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('friend_id');
            $table->string('photo', 100);
            $table->text('emotions');
            $table->timestamps();
        });

    }
}
