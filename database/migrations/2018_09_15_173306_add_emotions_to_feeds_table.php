<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmotionsToFeedsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('feeds', function ($table) {
            $table->tinyInteger('emotion');
            $table->tinyInteger('imageEmotion')->nullable();
            $table->tinyInteger('textEmotion');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('feeds', function ($table) {
            $table->dropColumn('emotion');
            $table->dropColumn('imageEmotion');
            $table->dropColumn('textEmotion');
        });
	}
}
