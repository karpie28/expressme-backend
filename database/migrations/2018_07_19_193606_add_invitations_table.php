<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvitationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        Schema::create('invitations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id_inviting')->unsigned();
            $table->integer('user_id_invited')->unsigned();
            $table->enum('status', ['accepted', 'rejected', 'blocked', 'waiting']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id_inviting')
                ->references('id')->on('users');
            $table->foreign('user_id_invited')
                ->references('id')->on('users');
        });



    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('invitations');
    }
}
