<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'feeds', function (Blueprint $table) {
            $table->increments('feed_id');
            $table->integer('user_id')->unsigned();
            $table->text('description');
            $table->text('image')->nullable();
            $table->timestamps();
            $table->softDeletes();


        });

        Schema::table('feeds', function($table)
        {
            $table->foreign('user_id')
                ->references('user_id')->on(config('app.db_prefix') . 'user')
                ->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'feeds');
    }
}
