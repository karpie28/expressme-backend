{
    "swagger": "2.0",
    "info": {
        "title": "This is documentation of expressme.pl api",
        "description": "Remember to use only HTTPS!",
        "contact": {
            "email": "contact@expressme.pl"
        },
        "version": "1.0.0"
    },
    "host": "api.expressme.pl",
    "basePath": "/",
    "schemes": [
        "https"
    ],
    "paths": {
        "/api/auth/login": {
            "post": {
                "tags": [
                    "/api/auth"
                ],
                "summary": "Login user.",
                "description": "This method logins user and returns his data.\r\n     *      access_token default has 12 hours lifetime.\r\n     *     To keep signed in after that time, please POST at api/auth/refresh",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "email",
                        "in": "formData",
                        "description": "The user email for login",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "password",
                        "in": "formData",
                        "description": "Password of user",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "400": {
                        "description": "Something went wrong"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "200": {
                        "description": "Succesfull operation.",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                }
            }
        },
        "/api/auth/register": {
            "post": {
                "tags": [
                    "/api/auth"
                ],
                "summary": "Register user.",
                "description": "This method registers user and returns his data.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "email",
                        "in": "formData",
                        "description": "The user email for login",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "password",
                        "in": "formData",
                        "description": "Password of user",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "name",
                        "in": "formData",
                        "description": "Name of user",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "surname",
                        "in": "formData",
                        "description": "Surname of user",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "birth_date",
                        "in": "formData",
                        "description": "BirthDate. example: 1996-08-19 00:00:00",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "401": {
                        "description": "Unauthorized"
                    },
                    "200": {
                        "description": "Succesfull operation.",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                }
            }
        },
        "/api/me": {
            "get": {
                "tags": [
                    "/api"
                ],
                "summary": "Get the authenticated User.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "Succesfull operation.",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                }
            },
            "post": {
                "tags": [
                    "/api"
                ],
                "summary": "Edit user profile.",
                "description": "This method updates user profile.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "name",
                        "in": "formData",
                        "description": "The user name",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "surname",
                        "in": "formData",
                        "description": "The user surname",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "avatar",
                        "in": "formData",
                        "description": "The user avatar",
                        "type": "file"
                    }
                ],
                "responses": {
                    "400": {
                        "description": "Validation error.",
                        "schema": {
                            "format": "string",
                            "example": "Validation error."
                        }
                    },
                    "200": {
                        "description": "Succesfull operation.",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                }
            }
        },
        "/api/auth/logout": {
            "post": {
                "tags": [
                    "/api/auth"
                ],
                "summary": "Logout user.",
                "description": "Log the user out (Invalidate the token).",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "Authorization key must have 'Bearer ' prefix.",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successfully logged out"
                    }
                }
            }
        },
        "/api/auth/refresh": {
            "post": {
                "tags": [
                    "/api/auth"
                ],
                "summary": "Refresh a token.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "Authorization key must have 'Bearer ' prefix.",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful operation."
                    }
                }
            }
        },
        "/api/auth/activate/{activate_token}": {
            "get": {
                "tags": [
                    "/api/auth"
                ],
                "summary": "Authenticate the user.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "404": {
                        "description": "User not found"
                    },
                    "200": {
                        "description": "User activated."
                    }
                }
            }
        },
        "/api/auth/activate/resend": {
            "post": {
                "tags": [
                    "/api/auth"
                ],
                "summary": "Resend authentcation code.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "email",
                        "in": "formData",
                        "description": "The user email",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "404": {
                        "description": "User not found"
                    },
                    "200": {
                        "description": "Email sent."
                    }
                }
            }
        },
        "/api/auth/password/reset": {
            "post": {
                "tags": [
                    "/api/auth"
                ],
                "summary": "Get reset password.",
                "description": "This method sends email with reset password link.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "email",
                        "in": "formData",
                        "description": "Email adress for reset password.",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "400": {
                        "description": "User not found.",
                        "schema": {
                            "format": "string",
                            "example": "User not found"
                        }
                    },
                    "200": {
                        "description": "Succesfull operation.",
                        "schema": {
                            "format": "string",
                            "example": "Email sent to reset password"
                        }
                    }
                }
            }
        },
        "/api/auth/password/change/{token}": {
            "post": {
                "tags": [
                    "/api/auth"
                ],
                "summary": "Post reset password.",
                "description": "This method resets user password.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "password",
                        "in": "formData",
                        "description": "The user password",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "400": {
                        "description": "Token not found.",
                        "schema": {
                            "format": "string",
                            "example": "Token not found"
                        }
                    },
                    "200": {
                        "description": "Succesfull operation.",
                        "schema": {
                            "format": "string",
                            "example": "Email sent to reset password"
                        }
                    }
                }
            }
        },
        "/api/chat/messages/{userIdFrom}": {
            "get": {
                "tags": [
                    "/api/chat"
                ],
                "summary": "Get all user feeds",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "Authorization key must have 'Bearer ' prefix.",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "limit",
                        "in": "query",
                        "type": "integer"
                    },
                    {
                        "name": "offset",
                        "in": "query",
                        "type": "integer"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Messages.",
                        "schema": {
                            "$ref": "#/definitions/Message"
                        }
                    },
                    "400": {
                        "description": "Bad validator."
                    }
                }
            }
        },
        "/api/chat/messages/{userIdToSend}": {
            "post": {
                "tags": [
                    "/api/chat"
                ],
                "summary": "Post message to User",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "Authorization key must have 'Bearer ' prefix.",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "message",
                        "in": "formData",
                        "description": "TExt of message",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "image",
                        "in": "formData",
                        "description": "File as image. Only jpg, jpeg, png. Max size: 3 MB",
                        "type": "file"
                    }
                ],
                "responses": {
                    "201": {
                        "description": "status."
                    }
                }
            }
        },
        "/api/feeds": {
            "get": {
                "tags": [
                    "/api/feeds"
                ],
                "summary": "Get all user feeds",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "Authorization key must have 'Bearer ' prefix.",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "limit",
                        "in": "query",
                        "type": "integer"
                    },
                    {
                        "name": "offset",
                        "in": "query",
                        "type": "integer"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Feeds.",
                        "schema": {
                            "$ref": "#/definitions/Feed"
                        }
                    },
                    "400": {
                        "description": "Bad validator."
                    }
                }
            },
            "post": {
                "tags": [
                    "/api/feeds"
                ],
                "summary": "Post feed",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "Authorization key must have 'Bearer ' prefix.",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "description",
                        "in": "formData",
                        "description": "TExt of feed",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "image",
                        "in": "formData",
                        "description": "File as image. Only jpg, jpeg, png. Max size: 3 MB",
                        "required": true,
                        "type": "file"
                    }
                ],
                "responses": {
                    "201": {
                        "description": "Feeds.",
                        "schema": {
                            "$ref": "#/definitions/Feed"
                        }
                    }
                }
            }
        },
        "/api/feeds/{feed_id}/like": {
            "get": {
                "tags": [
                    "/api/feeds"
                ],
                "summary": "Get likes of feed",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "Authorization key must have 'Bearer ' prefix.",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "feed_id",
                        "in": "path",
                        "description": "Feed id",
                        "required": true,
                        "type": "number"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Feeds."
                    },
                    "404": {
                        "description": "No such feed correlated with your friends."
                    }
                }
            },
            "post": {
                "tags": [
                    "/api/feeds"
                ],
                "summary": "Like feed",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "Authorization key must have 'Bearer ' prefix.",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "feed_id",
                        "in": "path",
                        "description": "Feed id",
                        "required": true,
                        "type": "number"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Feeds."
                    },
                    "404": {
                        "description": "No such feed correlated with your friends."
                    }
                }
            }
        },
        "/api/feeds/{feed_id}/comment": {
            "post": {
                "tags": [
                    "/api/feeds"
                ],
                "summary": "Comment feed",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "Authorization key must have 'Bearer ' prefix.",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "Feed id",
                        "in": "path",
                        "description": "Feed id",
                        "required": true,
                        "type": "number"
                    },
                    {
                        "name": "description",
                        "in": "path",
                        "description": "description",
                        "required": true,
                        "type": "number"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Feeds."
                    },
                    "400": {
                        "description": "Validation Error"
                    },
                    "404": {
                        "description": "No such feed correlated with your friends."
                    }
                }
            }
        },
        "/api/friends/search": {
            "get": {
                "tags": [
                    "/api/friends"
                ],
                "summary": "Search user by name.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "Authorization key must have 'Bearer ' prefix.",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "search",
                        "in": "formData",
                        "description": "String of searching value",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Users with given string.",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                }
            }
        },
        "/api/friends/invitation": {
            "get": {
                "tags": [
                    "/api/friends"
                ],
                "summary": "Get all invitations that are sent to you",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "Succesfull operation. Invitation accepted, Friend Added."
                    }
                }
            },
            "post": {
                "tags": [
                    "/api/friends"
                ],
                "summary": "Send invitation to given Id.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "Authorization key must have 'Bearer ' prefix.",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "user_id_invited",
                        "in": "formData",
                        "description": "Id of user you want to invite.",
                        "required": true,
                        "type": "number"
                    }
                ],
                "responses": {
                    "400": {
                        "description": "There was a problem with this invitation"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "200": {
                        "description": "Succesfull operation. Invitation accepted, Friend Added."
                    }
                }
            }
        },
        "/api/friends/invitation/{invitationId}/accept": {
            "put": {
                "tags": [
                    "/api/friends"
                ],
                "summary": "Accept invitation with given Id.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "Authorization key must have 'Bearer ' prefix.",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "404": {
                        "description": "There is no such invitation"
                    },
                    "400": {
                        "description": "There was a problem with this invitation"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "200": {
                        "description": "Succesfull operation. Invitation accepted, Friend Added."
                    }
                }
            }
        },
        "/api/friends/invitation/{invitationId}/reject": {
            "put": {
                "tags": [
                    "/api/friends"
                ],
                "summary": "Reject invitation with given Id.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "Authorization key must have 'Bearer ' prefix.",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "404": {
                        "description": "There is no such invitation"
                    },
                    "400": {
                        "description": "There was a problem with this invitation"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "200": {
                        "description": "Succesfull operation."
                    }
                }
            }
        },
        "/api/friends/invitation/{invitationId}/block": {
            "put": {
                "tags": [
                    "/api/friends"
                ],
                "summary": "Block invitation with given Id.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "Authorization key must have 'Bearer ' prefix.",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "404": {
                        "description": "There is no such invitation"
                    },
                    "400": {
                        "description": "There was a problem with this invitation"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "200": {
                        "description": "Succesfull operation."
                    }
                }
            }
        },
        "/api/friends/{friendId]": {
            "get": {
                "tags": [
                    "/api/friends"
                ],
                "summary": "Get friend with given Id.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "Authorization key must have 'Bearer ' prefix.",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "404": {
                        "description": "There is no such friend"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "200": {
                        "description": "Succesfull operation.",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                }
            },
            "delete": {
                "tags": [
                    "/api/friends"
                ],
                "summary": "Delete friend with given Id.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "Authorization key must have 'Bearer ' prefix.",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "404": {
                        "description": "There is no such friend"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "200": {
                        "description": "Succesfull operation."
                    }
                }
            }
        },
        "/api/friends": {
            "get": {
                "tags": [
                    "/api/friends"
                ],
                "summary": "Get all friends",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "Authorization key must have 'Bearer ' prefix.",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "401": {
                        "description": "Unauthorized"
                    },
                    "200": {
                        "description": "Succesfull operation.",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "Feed": {
            "required": [
                "description"
            ],
            "properties": {
                "description": {
                    "description": "Feed content",
                    "type": "string",
                    "example": "Great feed"
                },
                "user_id": {
                    "description": "",
                    "type": "number"
                },
                "image": {
                    "description": "Absolute URL to image",
                    "type": "string"
                },
                "User": {
                    "description": "Full user object",
                    "type": "string"
                },
                "emotion": {
                    "description": "Overall emotion based on scale 1-5, where 1 means really sad, 5 means really happy.\r\n     *         3 means neutral, but when photo is happy, and text is sad, then result will be neutral",
                    "type": "number"
                },
                "imageEmotion": {
                    "description": "Null, when there was no image, or image emotion based on scale 1-5, where 1 means really sad, 5 means really happy.",
                    "type": "number"
                },
                "textEmotion": {
                    "description": "textEmotion based on scale 1-5, where 1 means really sad, 5 means really happy.",
                    "type": "number"
                },
                "height": {
                    "description": "Height of image when present, if not then null.",
                    "type": "number"
                },
                "width": {
                    "description": "Height of image when present, if not then null.",
                    "type": "number"
                },
                "created_at": {
                    "description": "",
                    "type": "string"
                },
                "updated_at": {
                    "description": "",
                    "type": "string"
                }
            },
            "type": "object",
            "xml": {
                "name": "Feed"
            }
        },
        "FeedComment": {
            "required": [
                "description"
            ],
            "type": "object",
            "xml": {
                "name": "Feed"
            }
        },
        "FeedLike": {
            "required": [
                "description"
            ],
            "type": "object",
            "xml": {
                "name": "Feed"
            }
        },
        "Message": {
            "required": [
                "description"
            ],
            "properties": {
                "user_id": {
                    "description": "",
                    "type": "number"
                },
                "user_sent_id": {
                    "description": "Bigger id of user chat",
                    "type": "string"
                },
                "text": {
                    "description": "Message text",
                    "type": "string"
                },
                "emotion": {
                    "description": "Overall emotion based on scale 1-5, where 1 means really sad, 5 means really happy.\r\n     *         3 means neutral, but when photo is happy, and text is sad, then result will be neutral",
                    "type": "number"
                },
                "imageEmotion": {
                    "description": "Null, when there was no image, or image emotion based on scale 1-5, where 1 means really sad, 5 means really happy.",
                    "type": "number"
                },
                "textEmotion": {
                    "description": "textEmotion based on scale 1-5, where 1 means really sad, 5 means really happy.",
                    "type": "number"
                },
                "created_at": {
                    "description": "",
                    "type": "string"
                },
                "updated_at": {
                    "description": "",
                    "type": "string"
                }
            },
            "type": "object",
            "xml": {
                "name": "Message"
            }
        },
        "User": {
            "required": [
                "user_id",
                "email",
                "name"
            ],
            "properties": {
                "id": {
                    "description": "The user identifier.",
                    "type": "integer",
                    "format": "int64"
                },
                "email": {
                    "description": "The user email.",
                    "type": "string",
                    "example": "example@mail.com"
                },
                "name": {
                    "description": "",
                    "type": "string"
                },
                "surname": {
                    "description": "",
                    "type": "string"
                },
                "birth_date": {
                    "description": "",
                    "type": "string"
                },
                "avatar": {
                    "description": "",
                    "type": "string"
                },
                "last_emotion": {
                    "description": "",
                    "type": "integer"
                },
                "created_at": {
                    "description": "",
                    "type": "string"
                },
                "updated_at": {
                    "description": "",
                    "type": "string"
                }
            },
            "type": "object",
            "xml": {
                "name": "User"
            }
        }
    }
}