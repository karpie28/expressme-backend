<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'auth'], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@create');
    Route::post('activate/resend', 'AuthController@resendActivateToken');
    Route::get('activate/{activate_token}', 'AuthController@activateAccount');
    Route::post('/password/reset', 'AuthController@passwordReset');
    Route::post('/password/change/{token}', 'AuthController@passwordChange');

    Route::post('refresh', 'AuthController@refresh')->middleware('auth:api');
    Route::post('logout', 'AuthController@logout')->middleware('auth:api');

});

Route::get('me', 'AuthController@me')->middleware('auth:api');
Route::post('me', 'AuthController@updateMe')->middleware('auth:api');


Route::group(['middleware' => 'auth:api', 'prefix' => 'friends'], function ($router) {

    Route::get('/', 'FriendsController@listAllFriends');
    Route::get('/search', 'FriendsController@searchFriends');

    Route::get('/invitation', 'FriendsController@listAllWaitingInvites');
    Route::post('/invitation', 'FriendsController@sendInvite');
    Route::put('/invitation/{invitationId}/accept', 'FriendsController@acceptInvite');
    Route::put('/invitation/{invitationId}/reject', 'FriendsController@rejectInvite');
    Route::put('/invitation/{invitationId}/block', 'FriendsController@blockInvite');


    Route::get('/{friendId}', 'FriendsController@getFriend');
    Route::delete('/{friendId}', 'FriendsController@removeFriend');
});

Route::group(['middleware' => 'auth:api', 'prefix' => 'feeds'], function ($router) {

    Route::get('/', 'FeedsController@getMeFeeds');
    Route::post('/', 'FeedsController@postFeed');
    Route::get('/{feed_id}/like', 'FeedsController@likeFeedList');
    Route::post('/{feed_id}/like', 'FeedsController@likeFeed');
    Route::post('/{feed_id}/comment', 'FeedsController@addFeedComment');

});

Route::post('authorize', 'ChatsController@authorizeChat');


Route::group(['middleware' => 'auth:api', 'prefix' => 'chat'], function ($router) {
    Route::get('messages/{userIdFrom}', 'ChatsController@fetchMessages');
    Route::post('messages/{userToSend}', 'ChatsController@sendMessage');
});
